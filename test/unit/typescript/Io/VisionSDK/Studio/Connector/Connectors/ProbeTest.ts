/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Connector.Connectors {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import LiveContentArgumentType = Com.Wui.Framework.Services.Enums.LiveContentArgumentType;
    import UnitTestRunner = Com.Wui.Framework.UnitTestRunner;
    import FileSystemHandler = Com.Wui.Framework.Connector.Connectors.FileSystemHandler;
    import Terminal = Com.Wui.Framework.Localhost.Connectors.Terminal;
    import IUnitTestRunnerPromise = Com.Wui.Framework.IUnitTestRunnerPromise;

    export class ProbeTest extends UnitTestRunner {
        private probe : Probe;
        private fileSystem : FileSystemHandler;
        private terminal : Terminal;
        private testResourceRoot : string;
        private libraryPath : string;
        private generator : string;
        private sdkPath : string;

        constructor() {
            super();

            this.setMethodFilter("testFunctionalRun");
            this.probe = new Probe();
            this.sdkPath = "C:\\_workspace\\vision-sdk\\io-vision-sdk-studio-libs\\build\\releases\\1-1-0-WinGCC\\" +
                "static-win-gcc\\target\\resource\\libs\\VisionSDK";
        }

        public testFunctionalRun() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.probe.Load(this.libraryPath, ($status : boolean, $libPath : string) : void => {
                    assert.ok($status, "Can not load test library: " + $libPath);

                    this.probe.InvokeMethod("Create", LiveContentArgumentType.RETURN_INT, ($returnValue : number) : void => {
                        assert.equal($returnValue, 0);
                        this.probe.InvokeMethod("Validate", LiveContentArgumentType.RETURN_INT, ($returnValue : number) : void => {
                            assert.equal($returnValue, 0);
                            this.probe.InvokeMethod("Process", LiveContentArgumentType.RETURN_INT, {
                                    type : LiveContentArgumentType.IN_STRUCT,
                                    value: {
                                        fields  : {
                                            /* tslint:disable: object-literal-sort-keys */
                                            looped  : false,
                                            headless: true
                                            /* tslint:enable */
                                        },
                                        typeName: "ProcessOptions"
                                    }
                                },
                                ($returnValue : number) : void => {
                                    assert.equal($returnValue, 0);
                                    $done();
                                });
                        });
                    });
                });
            };
        }

        protected before() : IUnitTestRunnerPromise {
            this.timeoutLimit(150000);
            return ($done : () => void) : void => {
                this.testResourceRoot = this.getAbsoluteRoot() +
                    "/test/resource/data/Io/VisionSDK/Studio/Connector/Connectors/OpenVX_1_1/testFunctional";
                let args : string[];
                // todo get sdk online

                this.fileSystem = Loader.getInstance().getFileSystemHandler();
                this.terminal = Loader.getInstance().getTerminal();

                if (EnvironmentHelper.IsWindows()) {
                    // windows
                    this.generator = "MinGW Makefiles";
                    this.libraryPath = "VisionGraph.dll";
                    args = [
                        "-G", "\"MinGW Makefiles\"",
                        "-DCMAKE_BUILD_TYPE=Debug",
                        "-DVISION_SDK_PATH=\"" + this.sdkPath + "\"",
                        "-DTARGET_PLATFORM=win",
                        ".."
                    ];
                    process.env.PATH = this.fileSystem.NormalizePath(this.sdkPath + "/win_gcc/bins", true) +
                        ";" + process.env.PATH;
                } else {
                    // linux
                    this.generator = "Unix Makefiles";
                    this.libraryPath = "libVisionGraph.so";
                    args = [
                        "-G", "\"Unix Makefiles\"",
                        "-DCMAKE_BUILD_TYPE=Debug",
                        "-DVISION_SDK_PATH=\"" + this.sdkPath + "\"",
                        "-DTARGET_PLATFORM=linux",
                        ".."
                    ];
                    process.env.PATH = this.fileSystem.NormalizePath(this.sdkPath + "/win_gcc/bins", true) +
                        ":" + process.env.PATH;
                }
                this.libraryPath = this.testResourceRoot + "/build/" + this.libraryPath;

                if (this.fileSystem.Exists(this.libraryPath)) {
                    LogIt.Debug("Library already exists, build skipped.");
                    $done();
                } else {
                    if (this.fileSystem.CreateDirectory(this.testResourceRoot + "/build_cache")) {
                        this.terminal.Spawn(
                            "cmake",
                            args,
                            this.testResourceRoot + "/build_cache",
                            ($exitCode : number, $std : string[]) : void => {
                                if ($exitCode !== 0) {
                                    LogIt.Error("Build failed:\n" + $std[0] + $std[1]);
                                } else {
                                    this.terminal.Spawn(
                                        "cmake",
                                        ["--build", "."],
                                        this.testResourceRoot + "/build_cache",
                                        ($exitCode : number, $std : string[]) : void => {
                                            if ($exitCode === 0) {
                                                $done();
                                            } else {
                                                LogIt.Error("Can not build library:\n" + $std[0] + $std[1]);
                                            }
                                        }
                                    );
                                }
                            });
                    } else {
                        LogIt.Error("Failed to prepare build cache for test resource.");
                    }
                }
            };
        }
    }
}
