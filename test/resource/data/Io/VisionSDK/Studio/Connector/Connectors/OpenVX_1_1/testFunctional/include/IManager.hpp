/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef TESTCASE_1_1_IMANAGER_HPP_
#define TESTCASE_1_1_IMANAGER_HPP_

#ifdef WIN_PLATFORM
#define API_EXPORT __declspec(dllexport)
#define API_CALL __cdecl
#else
#define API_EXPORT
#define API_CALL
#endif

namespace TestCase_1_1 {
    /**
     * This structure defines options for processing routine.
     */
    struct ProcessOptions {
        bool looped;
        bool headless;
    };

    /**
     * This class defines interface for runtime manager.
     */
    class IManager {
     public:
        virtual int API_CALL Create() = 0;

        virtual int API_CALL Validate() = 0;

        virtual int API_CALL Process(const ProcessOptions &$options) = 0;

        virtual void API_CALL Stop() = 0;

        virtual bool API_CALL IsRunning() = 0;

        virtual void API_CALL setIsHeadless(bool $value) = 0;

        virtual void API_CALL setIsLooped(bool $value) = 0;
    };
}

#endif  // TESTCASE_1_1_IMANAGER_HPP_
