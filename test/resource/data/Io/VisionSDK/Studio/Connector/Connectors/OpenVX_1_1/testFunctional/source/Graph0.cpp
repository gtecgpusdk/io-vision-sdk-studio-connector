/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <fstream>

#include "BaseContext.hpp"
#include "BaseGraph.hpp"
#include "Graph0.hpp"
#include "VisualGraph.hpp"

namespace TestCase_1_1 {
    namespace VXUtils = Io::VisionSDK::Studio::Libs::VXUtils;

    std::vector<std::string> Graph0::testCasesplit(std::string::const_iterator it, std::string::const_iterator end,
                                           const std::regex &e) {
        std::smatch m{};
        std::vector<std::string> ret{};
        while (std::regex_search(it, end, m, e)) {
            ret.emplace_back(m.str());
            std::advance(it, m.position() + m.length());
        }
        return ret;
    }

    Graph0::Graph0(BaseContext *parent)
            : BaseGraph(parent) {
    }

    vx_status Graph0::create() {
        vx_status status = vxGetStatus((vx_reference)this->getVxGraph());
        if (status == VX_SUCCESS) {
            this->vxDataMap["vxData0"] = this->createImage(this->getParent()->getVxContext(), 640, 480, VX_DF_IMAGE_U8);
            this->getParent()->Check(this->vxDataMap["vxData0"]);
            vx_uint32 vxData1Value = 4;
            this->vxDataMap["vxData1"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_UINT32, &vxData1Value);
            this->getParent()->Check(this->vxDataMap["vxData1"]);
            this->vxDataMap["vxData2"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData0"]), BaseGraph::getImageHeight(this->vxDataMap["vxData0"]), VX_DF_IMAGE_S16);
            this->getParent()->Check(this->vxDataMap["vxData2"]);
            vx_float32 vxData3Value = 0.500f;
            this->vxDataMap["vxData3"] = (vx_reference)vxCreateScalar(this->getParent()->getVxContext(), VX_TYPE_FLOAT32, &vxData3Value);
            this->getParent()->Check(this->vxDataMap["vxData3"]);
            this->vxDataMap["vxData4"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData0"]), BaseGraph::getImageHeight(this->vxDataMap["vxData0"]), VX_DF_IMAGE_U8);
            this->getParent()->Check(this->vxDataMap["vxData4"]);
            this->vxDataMap["vxData5"] = this->createImage(this->getParent()->getVxContext(), BaseGraph::getImageWidth(this->vxDataMap["vxData0"]), BaseGraph::getImageHeight(this->vxDataMap["vxData0"]), VX_DF_IMAGE_S16);
            this->getParent()->Check(this->vxDataMap["vxData5"]);
            this->vxNodesMap["acc_squ1"] = (vx_reference)vxAccumulateSquareImageNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData0"], (vx_scalar)this->vxDataMap["vxData1"], (vx_image)this->vxDataMap["vxData2"]);
            this->getParent()->Check(this->vxNodesMap["acc_squ1"]);
            this->vxNodesMap["acc_wei1"] = (vx_reference)vxAccumulateWeightedImageNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData0"], (vx_scalar)this->vxDataMap["vxData3"], (vx_image)this->vxDataMap["vxData4"]);
            this->getParent()->Check(this->vxNodesMap["acc_wei1"]);
            this->vxNodesMap["acc"] = (vx_reference)vxAccumulateImageNode(this->getVxGraph(), (vx_image)this->vxDataMap["vxData0"], (vx_image)this->vxDataMap["vxData5"]);
            this->getParent()->Check(this->vxNodesMap["acc"]);
        }
        return status;
    }

    vx_status Graph0::validate() {
        vx_status status = vxGetStatus((vx_reference)this->getVxGraph());
        if (status == VX_SUCCESS) {
            status = this->getParent()->Check(vxVerifyGraph(this->getVxGraph()));
        }
        return status;
    }

    vx_status Graph0::process() const {
        vx_status status = vxGetStatus((vx_reference)this->getVxGraph());
        int i = 0;
        VXUtils::Utils::StopWatch stopWatch{};
        do {
            std::string vxDataName;
            std::ifstream ifs("build\\runtime.config");
            std::string str;
            ifs >> str;
            ifs.close();
            auto items = testCasesplit(str.begin(), str.end(), std::regex(R"(\w+=[\w\d"_-]+)"));
            for (const auto &item : items) {
                auto index = item.find("run=");
                if (index == 0) {
                    std::string tmp = item.substr(4);
                    if (tmp != "true") {
                        const_cast<VisualGraph *>(this->getParent()->getParent())->setIsLooped(false);
                    }
                }
                index = item.find("vxDataName=");
                if (index == 0) {
                    vxDataName = item.substr(11);
                    if (vxDataName[0] == '"') {
                        vxDataName = vxDataName.substr(1, vxDataName.size() - 2);
                    }
                }
            }
            stopWatch.Start();
            if (status == VX_SUCCESS) {
                auto ioCom0 = this->getParent()->getParent()->getIoCom("ioCom0");
                if (i == 0) {
                    vx_image vxData0Ref = (vx_image)this->getData("vxData0");
                    ioCom0->getFrame(vxData0Ref, true);
                }
            }
            if (status == VX_SUCCESS) {
                status = this->getParent()->Check(vxProcessGraph(this->getVxGraph()));
            }
            if (status == VX_SUCCESS) {
                auto ioCom1 = this->getParent()->getParent()->getIoCom("ioCom1");
                vx_image vxData2Ref = (vx_image)this->getData("vxData2");
                ioCom1->setFrame(vxData2Ref);
            }
            if (status == VX_SUCCESS) {
                auto ioCom2 = this->getParent()->getParent()->getIoCom("ioCom2");
                vx_image vxData4Ref = (vx_image)this->getData("vxData4");
                ioCom2->setFrame(vxData4Ref);
            }
            if (status == VX_SUCCESS) {
                auto ioCom3 = this->getParent()->getParent()->getIoCom("ioCom3");
                vx_image vxData5Ref = (vx_image)this->getData("vxData5");
                ioCom3->setFrame(vxData5Ref);
            }
            stopWatch.Stop();
            std::cout << "Processing of graph: \"Graph0\" spend (" << i << "): " << stopWatch.getElapsed() << " ms" << std::endl;
            if (this->getParent()->getParent()->IsHeadless() && !vxDataName.empty()) {
                VXUtils::Streaming::VxImageStreamer::SendQuery(this->getParent()->getVxContext(), (vx_image)this->getData(vxDataName));
            }
        } while ((this->loopCondition(i++) || this->getParent()->getParent()->IsLooped()) && (status == VX_SUCCESS));
        return status;
    }

    bool Graph0::loopCondition(int $loopCnt) const {
        return false;
    }
}
