/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef TESTCASE_1_1_REFERENCE_HPP_
#define TESTCASE_1_1_REFERENCE_HPP_

namespace TestCase_1_1 {
    class VisualGraph;

    class BaseContext;

    class BaseGraph;

    class Context0;

    class Graph0;
}

#endif  // TESTCASE_1_1_REFERENCE_HPP_
