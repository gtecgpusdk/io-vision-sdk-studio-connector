/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Io.VisionSDK.Studio.Connector.Connectors {
    "use strict";
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import LiveContentArgumentType = Com.Wui.Framework.Services.Enums.LiveContentArgumentType;
    import FFIOptions = Com.Wui.Framework.Services.Connectors.FFIOptions;

    export class Probe extends Com.Wui.Framework.Connector.Connectors.FFIProxy {
        public static Load($fileOrOptions : string | FFIOptions,
                           $callback? : (($status : boolean, $libraryPath : string) => void) | IResponse) : void {
            this.getInstance<Probe>().Load($fileOrOptions, ResponseFactory.getResponse($callback));
        }

        public static InvokeMethod($name : string, $returnType : any, ...$args : any[]) : void {
            const instance : Probe = this.getInstance();
            instance.InvokeMethod.apply(instance, [$name, $returnType].concat($args));
        }

        public static Release($callback? : (($path : string) => void) | IResponse) : void {
            this.getInstance<Probe>().Release();
            ResponseFactory.getResponse($callback).Send(LiveContentArgumentType.RETURN_VOID);
        }
    }
}
